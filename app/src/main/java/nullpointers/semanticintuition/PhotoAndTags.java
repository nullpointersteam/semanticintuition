package nullpointers.semanticintuition;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Achintya on 6/18/2016.
 */
public class PhotoAndTags extends SugarRecord {
    public String photoLocation;
    public String tags;

    public PhotoAndTags() {
    }

    public PhotoAndTags(String photoLocation, String tags) {
        this.photoLocation = photoLocation;
        this.tags = tags;
    }

    public static String[] getAllTags() {
        List<PhotoAndTags> photoAndTagsList = PhotoAndTags.listAll(PhotoAndTags.class);
        String buffer = new String();
        for (PhotoAndTags photoAndTags : photoAndTagsList) {
            buffer += photoAndTags.tags + " ";
        }
        //Converting array with repetitions into a set without repetitions
        Set<String> mySet = new HashSet<String>(Arrays.asList(buffer.split("\\s+")));
        return mySet.toArray(new String[mySet.size()]);
    }

    public static ArrayList<String> getFilenamesFromTags(String[] tags) {
        ArrayList<String> result = new ArrayList<>();
        String concatenatedTags = Arrays.toString(tags).replace(",", "").replace("[", "").replace("]", "");
        List<PhotoAndTags> photoAndTagsList = PhotoAndTags.listAll(PhotoAndTags.class);
        for (PhotoAndTags photoAndTags : photoAndTagsList) {
            for (String eachTag : (photoAndTags.tags).split(" ")) {
                if (concatenatedTags.contains(eachTag)) {
                    result.add(photoAndTags.photoLocation);
                    break;
                }
            }
        }
        return result;
    }
}