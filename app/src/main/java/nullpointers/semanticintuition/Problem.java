package nullpointers.semanticintuition;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.eftimoff.viewpagertransformers.*;

public class Problem extends AppCompatActivity {


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private ViewPager mViewPager;
    private int numberOfPages = 3;
    private int mCurrentTabPosition;
    private static ProblemData problemData;
    private static ArrayList<String> picturesInDB = new ArrayList<>();
    private String problemDescription;
    //private static Set<String> problemPictures = new LinkedHashSet<>();

    //String array contains (URI, Answer)
    private static SparseArrayCompat<String[]> imagesAnswers = new SparseArrayCompat<String[]>();
    private static String imageURLSignature;
    //private static String[] localImages;
    private static String allTags;

    //GUI elements in fragment
    private TextView problemDescTextView;
    private TextView imagesTagsTextView;
    private EditText answerEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem);

        //cast GUI elements
        problemDescTextView = (TextView) findViewById(R.id.problemDescTextView);
        imagesTagsTextView = (TextView) findViewById(R.id.imagesTagsTextView);
        answerEditText = (EditText) findViewById(R.id.answerEditText);

        //New signature to generate new images everytime the view is created
        imageURLSignature = ProblemData.CurrentDateTime();

        //Load problem data from intent
        problemData = ProblemData.loadProblemDataFromIntent(this);
        String[] tags = problemData.getTagsArray();
        picturesInDB = PhotoAndTags.getFilenamesFromTags(tags);

        //We load the tags here, if they are empty we create some random ones
        allTags = problemData.problemTags;
        if (allTags != null && !allTags.isEmpty() && !allTags.equalsIgnoreCase("[]")) {
            //Remove the "[" and the "]", get a comma-separated list of tags for the url
            allTags = allTags.substring(1, allTags.length() - 1);
            //Remove all empty spaces
            allTags = allTags.replaceAll("\\s", "");
        } else {
            //Here there must be a better way to get random images, like choosing of a pool of random words
            //allTags = "Random,Image,Picture,Beach,Animal";
            allTags = RandomNouns.getRandomNouns(5); //We get 5 random nouns
            //Log.d("boo", allTags.toString());
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPageTransformer(false, new RotateDownTransformer());

        ViewPager.SimpleOnPageChangeListener mPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(final int position) {
                //Log.d("imageDebug", "TabChange detected, we are in " + mCurrentTabPosition + " and we go to " + position);
                onTabChanged(mSectionsPagerAdapter, mCurrentTabPosition, position);
                //save the answer in the SparceArray
                putItemToImagesAnswers(imagesAnswers, mCurrentTabPosition, 1, answerEditText.getText().toString());
                //Load the answer of the other section
                String answer = "";
                //We see if we have an answer on the other tab
                if (imagesAnswers.get(position) != null)
                    answer = imagesAnswers.get(position)[1];
                answerEditText.setText(answer);
                if (answer != null)
                    if (!answer.isEmpty())
                        answerEditText.setSelection(answer.length());

                //Change the position of the tab
                mCurrentTabPosition = position;
            }
        };

        mViewPager.addOnPageChangeListener(mPageChangeListener);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Saving all in DB", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
                Intent next = new Intent(Problem.this, ShowResults.class);
                next.putExtra("problemDescription", problemDescription);
                next.putExtra("tags", allTags);
                saveProblemData(false);
                if (problemData.answers.isEmpty())
                    return; //We dont continue if the answers are empty
                problemData.loadProblemDataToIntent(next);
                startActivity(next);
            }
        });

        loadProblemData();
    }

    @Override
    public void onBackPressed() {
        //Here we have to input a save mechanism
        saveProblemData(true);
        super.onBackPressed();
    }

    /**
     * Method to add a new fragment instance when we are on the second-last element
     *
     * @param adapter
     * @param oldPosition
     * @param newPosition
     */
    protected void onTabChanged(final SectionsPagerAdapter adapter, final int oldPosition, final int newPosition) {
        if (newPosition >= (numberOfPages - 1)) {
            //Log.d("imageDebug", "We changed numberOfPages from " + numberOfPages + " to " + (numberOfPages + 1));
            numberOfPages++;
        }
        adapter.notifyDataSetChanged();
        //mViewPager.setCurrentItem(newPosition);
    }

    @Override
    protected void onStop() {
        super.onStop(); // Always call the superclass method first
        saveProblemData(true); //Save the data, before app is killed
    }

    private void saveProblemData(boolean isGoingBack) {
        //We get the last answer
        String lastAnswer = answerEditText.getText().toString();
        if (!lastAnswer.isEmpty())
            putItemToImagesAnswers(imagesAnswers, mCurrentTabPosition, 1, lastAnswer);

        //index 0 for image path (URI)
        problemData.picturesPaths = DataManipulator.getStringFromImageAnswers(imagesAnswers, 0);
        //index 1 for answers
        problemData.answers = DataManipulator.getStringFromImageAnswers(imagesAnswers, 1);
        Toast toast;
        if (problemData.answers.isEmpty()) {
            if (!isGoingBack) {
                toast = Toast.makeText(getApplicationContext(), "Please write at least an answer on a loaded image", Toast.LENGTH_SHORT);
                toast.show();
            }
        } else {
            toast = Toast.makeText(getApplicationContext(), "Saving data", Toast.LENGTH_SHORT);
            toast.show();
            problemData.saveData();
        }
    }


    private void loadProblemData() {
        problemData.reloadFromDatabase();
        problemDescTextView.setText(problemData.problemDescription);
        problemDescription = problemData.problemDescription;
        imagesTagsTextView.setText(problemData.problemTags);
        //Reset imagesAnswers
        imagesAnswers = new SparseArrayCompat<String[]>();
        if (problemData.creationState.equalsIgnoreCase("modify")) {
            imagesAnswers = DataManipulator.getSparceArrayFromStrings(problemData.getPicturesPathsArray(),
                    problemData.getAnswersArray());
            if (imagesAnswers.get(0) != null)
                answerEditText.setText(imagesAnswers.get(0)[1]);
        } else if (problemData.creationState.equalsIgnoreCase("new")) {
            String[] localImages = picturesInDB.toArray(new String[0]);
            imagesAnswers = DataManipulator.getSparceArrayFromStrings(localImages,
                    new String[localImages.length]);
        }
    }

    private static void putItemToImagesAnswers(SparseArrayCompat<String[]> imagesAnswers, int position, int index, String textToPut) {
        String[] stringToPut;
        if (index == 0) {
            String otherString = (imagesAnswers.get(position) == null) ? "" : imagesAnswers.get(position)[1];
            stringToPut = new String[]{textToPut, otherString};
        } else {
            String otherString = (imagesAnswers.get(position) == null) ? "" : imagesAnswers.get(position)[0];
            stringToPut = new String[]{otherString, textToPut};
        }
        imagesAnswers.put(position, stringToPut);
    }

    private static boolean isItemNullOrEmpty(SparseArrayCompat<String[]> imagesAnswers, int position, int index) {
        if (imagesAnswers.get(position) == null) {
            return true;
        } else if (imagesAnswers.get(position)[index] == null) {
            return true;
        } else {
            return imagesAnswers.get(position)[index].isEmpty();
        }
    }


    //INNER CLASSES: SectionPagesAdapter, PlaceholderFragment, BitmapImageViewTargetAndSection


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //Log.d("imageDebug", "We created a new fragment instance");
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show total pages.
            return numberOfPages;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Integer.toString(position);
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.problem_swipe_fragment, container, false);
            final ImageView glideImageView = (ImageView) rootView.findViewById(R.id.glideImageLoader);

            //section -1
            int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
            //Log.d("imageDebug", "Fragment instance OnCreateView, sectionNumber " + sectionNumber);

            final String pathToLoad;
            //We check if there are local images, and we load them
            if (!isItemNullOrEmpty(imagesAnswers, sectionNumber, 0)) {
                pathToLoad = imagesAnswers.get(sectionNumber)[0];
                loadImage(this.getContext().getApplicationContext(),glideImageView,pathToLoad);
            } else { //At this point we are sure it will be an image from internet
                pathToLoad = "http://loremflickr.com/320/240/" + allTags + "?" + imageURLSignature + "?" + sectionNumber;
                loadImageFromInternet(this.getContext().getApplicationContext(),glideImageView,pathToLoad, sectionNumber, imagesAnswers);
            }
            return rootView;
        }
    }

    public static void loadImage(Context context, ImageView imageView, String pathToLoad) {
        Glide.with(context).load(pathToLoad).placeholder(R.drawable.animloading).crossFade().into(imageView);
    }

    public static void loadImageFromInternet(final Context context, final ImageView imageView, String pathToLoad,
                                             final int sectionNumber, final SparseArrayCompat<String[]> imagesAnswers) {
        Glide.with(context).load(R.drawable.animloading).into(imageView); //I hope this is blocking
        ImageRequest request = new ImageRequest(pathToLoad,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        //mImageView.setImageBitmap(bitmap);
                        //glideImageView.setImageBitmap(bitmap);
                        Uri uri = saveBitmap(bitmap,context,sectionNumber,imagesAnswers);
                        Glide.with(context).load(uri).crossFade().into(imageView);
                    }
                }, 0, 0, ImageView.ScaleType.CENTER, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Glide.with(context).load(R.drawable.errorloading).crossFade().into(imageView);
                    }
                });

        RequestSingleton.getInstance(context).addToRequestQueue(request);
    }


    public static Uri saveBitmap(Bitmap resource, Context context, int sectionNumber, SparseArrayCompat<String[]> imagesAnswers) {
        FileOutputStream outputStreamFile = null;
        File outputFile;
        File outputFolder;
        Uri uri = null;
        try {
            String outputFilename = ProblemData.CurrentDateTime() + sectionNumber + "-" + ".jpg";
            outputFolder = new File(context.getFilesDir() + File.separator +
                    "InternetImages");
            if (!outputFolder.exists()) {
                if (!outputFolder.mkdir()) {
                }//its a condition, but we use it to create a folder as well
                //Log.d("SIntution_Problem", "The folder was not created succesfully");
            }
            outputFile = new File(outputFolder + File.separator + outputFilename);
            outputStreamFile = new FileOutputStream(outputFile);
            uri = Uri.fromFile(outputFile);
            //Log.d("SIntution_Problem", "The URI is " + uri);
            resource.compress(Bitmap.CompressFormat.JPEG, 75, outputStreamFile); // Bitmap instance

            //modify the images answers
            putItemToImagesAnswers(imagesAnswers, sectionNumber, 0, uri.toString());
            //Log.d("imageDebug", "Saved bitmap to uri " + uri.toString() + " sectionNumber " + sectionNumber);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStreamFile != null) {
                    outputStreamFile.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return uri;
    }
}