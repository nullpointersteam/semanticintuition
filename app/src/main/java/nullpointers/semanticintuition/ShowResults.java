package nullpointers.semanticintuition;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class ShowResults extends AppCompatActivity {

    private ProblemData problemData;
    private FloatingActionButton sendToEmailFAB;
    private TextView problemTextView;
    private  TextView tagsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);

        //Cast GUI elements
        sendToEmailFAB = (FloatingActionButton) findViewById(R.id.sendToEmailFAB);
        problemTextView = (TextView) findViewById(R.id.problem);
        tagsTextView = (TextView)findViewById(R.id.tags);

        //Load problem data
        problemData = ProblemData.loadProblemDataFromIntent(this);

        //Now you have access to problemData.picturesPaths and problemData.answers
        //if you want you can load them into arrays or lists by doing this
        String problem = getIntent().getStringExtra("problemDescription");
        String tags = getIntent().getStringExtra("tags");
        problemTextView.setText("Problem: " + problem);
        tagsTextView.setText("Selected Tags: " + tags);
        String[] answers = problemData.getAnswersArray();
        String[] picturesPaths = problemData.getPicturesPathsArray();
        CustomList adapter = new CustomList(this, answers, picturesPaths);
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        //Log.e("Answer: " + answers[0], picturesPaths[0]);
        //of course those variables above have to be declared global in the class if you want to
        //use them outside of the OnCreate method

        //no need to check for empty answers or picturePaths, all the paths and answers that you get
        //are complete, so you can just use and iterator "i" that loops through all elements and build
        //your listview or whatever you have in mind for this part.

        //finally to send it through email, I gather all the answers into a string
        //and use the FloatingActionBar called sendToEmailFAB
        sendToEmailFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendMessage = Outputer.getIntentSendMessage(problemData.getAnswerOutputString(), view.getContext());
                startActivity(sendMessage);
            }
        });
    }
}
