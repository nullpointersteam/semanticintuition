package nullpointers.semanticintuition;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.clarifai.api.ClarifaiClient;
import com.clarifai.api.RecognitionRequest;
import com.clarifai.api.RecognitionResult;
import com.clarifai.api.Tag;
import com.clarifai.api.exception.ClarifaiException;
import com.cunoraz.tagview.OnTagClickListener;
import com.cunoraz.tagview.TagView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TagsAfterCameraActivity extends AppCompatActivity {

    static boolean Saved;
    ImageView clickedImageHolder;
    FloatingActionButton SaveButton;


    //Clarifai
    TagView tagGroup;
    private static final String TAG = TagsAfterCameraActivity.class.getSimpleName();

    private static final int CODE_PICK = 1;

    private final ClarifaiClient client = new ClarifaiClient(Credentials.CLIENT_ID,
            Credentials.CLIENT_SECRET);
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);
        Saved = false;

        clickedImageHolder = (ImageView) findViewById(R.id.captured_photo);
        SaveButton = (FloatingActionButton) findViewById(R.id.SaveButton);
        tagGroup = (TagView) findViewById(R.id.tag_groupFromClarifai);
        File[] fil = (new File(String.valueOf(getExternalFilesDir(null)))).listFiles();
        Arrays.sort(fil);


        String filename = getExternalFilesDir(null) + File.separator + fil[fil.length - 1].getName();
        Bitmap bitmap = decodeSampledBitmapFromFilename(filename, 0, 200, 200);
        if (bitmap != null) {
            //textView.setText("Recognizing...");
            Toast.makeText(this, "Recognizing...", Toast.LENGTH_SHORT).show();

            // Run recognition on a background thread since it makes a network call.
            new AsyncTask<Bitmap, Void, RecognitionResult>() {
                @Override
                protected RecognitionResult doInBackground(Bitmap... bitmaps) {
                    return recognizeBitmap(bitmaps[0]);
                }

                @Override
                protected void onPostExecute(RecognitionResult result) {
                    updateUIForResult(result);
                }
            }.execute(bitmap);
        } else {
            //textView.setText("Unable to load selected image.");
            Toast.makeText(this, "Unable to load selected image.", Toast.LENGTH_SHORT).show();
        }

        SaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), selectedTagsToString().toString(), Toast.LENGTH_LONG).show();
                EditText editText = ((EditText) findViewById(R.id.TagsBox));

                String tags = editText.getText().toString();
                tags = tags.replace("\"", "");
                try {
                    for (String eachTag : selectedTagsToString()) {
                        tags = tags + " " + eachTag;
                    }
                } catch (Exception e) {
                }

                PhotoAndTags photoAndTags = new PhotoAndTags(getLastFilename(), tags.trim());
                photoAndTags.save();
                Toast.makeText(getApplicationContext(), "Saved Successfully!", Toast.LENGTH_SHORT).show();
                Saved = true;
                finish();
            }
        });

        Glide.with(getApplicationContext())
                .load(filename)
                .placeholder(R.drawable.loading)
                .crossFade()
                .into(clickedImageHolder);
    }

    public Bitmap getImageFileFromSDCard(String filename) {
        Bitmap bitmap = null;
        File imageFile = new File(filename);
        try {
            FileInputStream fis = new FileInputStream(imageFile);
            bitmap = BitmapFactory.decodeStream(fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap decodeSampledBitmapFromFilename(String filename, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    private RecognitionResult recognizeBitmap(Bitmap bitmap) {
        try {
            // Scale down the image. This step is optional. However, sending large images over the
            // network is slow and  does not significantly improve recognition performance.
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 320,
                    320 * bitmap.getHeight() / bitmap.getWidth(), true);

            // Compress the image as a JPEG.
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            scaled.compress(Bitmap.CompressFormat.JPEG, 90, out);
            byte[] jpeg = out.toByteArray();

            // Send the JPEG to Clarifai and return the result.
            return client.recognize(new RecognitionRequest(jpeg)).get(0);
        } catch (ClarifaiException e) {
            //Log.e(TAG, "Clarifai error", e);
            return null;
        }
    }

    /**
     * Updates the UI by displaying tags for the given result.
     */
    private void updateUIForResult(RecognitionResult result) {
        if (result != null) {
            if (result.getStatusCode() == RecognitionResult.StatusCode.OK) {
                // Display the list of tags in the UI.
                StringBuilder b = new StringBuilder();
                for (Tag tag : result.getTags()) {
                    b.append(b.length() > 0 ? ", " : "").append(tag.getName());
                }
                //textView.setText("Tags:\n" + b);

                //Just writing all Tags to the TagsAfterCameraActivity Screen when user starts application
                try {
                    //gets all Tags as a ArrayList<List> and writes each string in one tag
                    //tag is added to the tagGroup and shown
                    for (Tag tagFromDB : result.getTags()) {
                        CustomTag tag = new CustomTag(tagFromDB.getName().replace(" ", "-"));
                        tagGroup.addTag(tag);
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Please take a pictures and enter some tags first", Toast.LENGTH_SHORT).show();
                }

                tagGroup.setOnTagClickListener(new OnTagClickListener() {
                    @Override
                    public void onTagClick(com.cunoraz.tagview.Tag tag, int position) {
                        ((CustomTag) tag).selectTag();
                        redrawTagView(tagGroup);
                    }
                });

            } else {
                //Log.e(TAG, "Clarifai: " + result.getStatusMessage());
                Toast.makeText(this, "Sorry, there was an error recognizing your image.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Sorry, there was an error recognizing your image.", Toast.LENGTH_LONG).show();
        }
    }

    //Perhaps redraws the tag upon clicking on it.
    private void redrawTagView(TagView tagGroup) {
        //redraw all tags
        List<com.cunoraz.tagview.Tag> tags = tagGroup.getTags();
        //We add a dummy tag and then remove it to redraw the tags
        tags.add(new com.cunoraz.tagview.Tag("dummyTag"));
        tagGroup.remove(tags.size() - 1);
    }

    //This returns a list of selected tags to the Save-button listener activity
    private List<String> selectedTagsToString() {
        List<String> selectedTags = new ArrayList<>();
        List<com.cunoraz.tagview.Tag> tags = tagGroup.getTags();
        for (com.cunoraz.tagview.Tag tag : tags) {
            if (((CustomTag) tag).isSelected) {
                selectedTags.add(tag.text);
            }
        }
        return selectedTags;
        /*
        Log.e("Stringoutput: ",selectedTags.toString());
        String[] selectedTagsArray = selectedTags.toArray(new String[0]);
        return Arrays.toString(selectedTagsArray);*/
    }

    public String getLastFilename() {
        File[] fil = (new File(String.valueOf(getExternalFilesDir(null)))).listFiles();
        Arrays.sort(fil);
        return String.valueOf(fil[fil.length - 1]);

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (!Saved) {
            PhotoAndTags photoAndTags = new PhotoAndTags(getLastFilename(), "");
            photoAndTags.save();
        }
    }


}

