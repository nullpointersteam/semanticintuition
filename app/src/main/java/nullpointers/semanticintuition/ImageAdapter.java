package nullpointers.semanticintuition;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;


public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<URI> uriArrayList;

    // Constructor
    public ImageAdapter(Context context, ArrayList<URI> uriArrayList) {
        mContext = context;
        this.uriArrayList = uriArrayList;
    }

    public int getCount() {
        return uriArrayList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            //imageView.setRotation(90);
            imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //imageView.setPadding(18, 18, 18, 18);
            try{
            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext(),AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                    LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                    View view = factory.inflate(R.layout.fragment_blank, null);

                    ImageView imageHolder = (ImageView) view.findViewById(R.id.SelectedImage);
                    //imageHolder.setRotation(90);
                    ArrayList<String> filenames = new ArrayList<String>();
                    for (File file : (new File(String.valueOf(mContext.getExternalFilesDir(null)))).listFiles()) {
                        filenames.add(file.getName());
                    }
                    Collections.sort(filenames);

                    Glide.with(mContext).load(new File(uriArrayList.get(position))).crossFade().into(imageHolder);

                    final PhotoAndTags photoAndTags = PhotoAndTags.findById(PhotoAndTags.class, position + 1);
                    String retrievedTags = photoAndTags.tags;

                    final EditText tagbox = (EditText) view.findViewById(R.id.RetrievedTags);

                    if (retrievedTags == "") {
                        tagbox.setHintTextColor(Color.parseColor("#000000"));
                        tagbox.setHint("No tags present!");
                    } else
                        tagbox.setText(retrievedTags);
                    alertbox.setView(view);
                    alertbox.setNeutralButton("Back to Photos",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0,
                                                    int arg1) {

                                }
                            });
                    alertbox.setPositiveButton("Save Changes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(mContext, "Saving changes...", Toast.LENGTH_SHORT).show();
                            String newTags = tagbox.getText().toString().replace("\"", "");
                            photoAndTags.tags = newTags;
                            photoAndTags.save();
                            dialog.dismiss();
                            Toast.makeText(mContext, "Changes successfully saved!", Toast.LENGTH_SHORT).show();

                        }
                    });
                    alertbox.show();
                    ;
                }

            });}
            catch (Exception e)
            {
                //Log.e("Error: ", e.getLocalizedMessage());
            }

        } else {
            imageView = (ImageView) convertView;
        }
        Glide.with(mContext).load(new File(uriArrayList.get(position))).into(imageView);
        return imageView;
    }

}
