package nullpointers.semanticintuition;

/**
 * Created by Moritz on 15/05/2016.
 */

import android.graphics.Color;

import com.cunoraz.tagview.Tag;

import java.util.ArrayList;
import java.util.Random;


// this class is just a collection of static methods that serve as properties for the tags
public class TagPropertiesClass {
    public String color;

    /**
     * Set the normal Tag properties
     *
     * @param tag Tag object that has to be changed
     * @return the changed tag
     */
    static public Tag setTAG(Tag tag) {

        tag.tagTextColor = Color.parseColor("#FFFFFF");
        tag.layoutColor = Color.parseColor("#3f51b5");
        tag.layoutColorPress = Color.parseColor("#3f51b5");
        tag.radius = 10f;
        tag.tagTextSize = 14f;
        tag.layoutBorderSize = 1f;
        tag.layoutBorderColor = Color.parseColor("#FFFFFF");
        tag.deleteIcon = "x";
        tag.isDeletable = true;
        return tag;
    }

    /**
     * Set the clicked Tag properties
     *
     * @param tag Tag object that has to be changed
     * @return the changed tag
     */
    static public Tag setchangedTAG(Tag tag) {

        String color = getRandomColor();

        tag.tagTextColor = Color.parseColor("#000000");
        tag.layoutColor = Color.parseColor("#ffc107");
        tag.layoutColorPress = Color.parseColor("#ffc107");
        tag.isDeletable = false;

        return tag;
    }

    /**
     * function will be maybe usefull in the future so I kept it
     *
     * @return a random color
     */
    static private String getRandomColor() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("#ED7D31");
        colors.add("#00B0F0");
        colors.add("#FF0000");
        colors.add("#D0CECE");
        colors.add("#00B050");
        colors.add("#9999FF");
        colors.add("#FF5FC6");
        colors.add("#FFC000");
        colors.add("#7F7F7F");
        colors.add("#4800FF");

        return colors.get(new Random().nextInt(colors.size()));
    }

    public String getColor() {
        return color;
    }

    public void setColor() {
        this.color = color;
    }

}

