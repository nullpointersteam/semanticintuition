package nullpointers.semanticintuition;

import com.cunoraz.tagview.Tag;

import android.graphics.Color;

/**
 * Created by Gabo10 on 09/07/2016.
 */
public class CustomTag extends Tag {

    public boolean isSelected = false;

    public CustomTag(String text) {
        super(text);
        setDefault();
    }

    public void setDefault() {
        this.isSelected = false;
        this.tagTextColor = Color.parseColor("#88D315");
        this.layoutColor = Color.parseColor("#033F4C");
        //this.layoutColorPress = Color.parseColor("#5AC0D9");
        this.radius = 10f;
        this.tagTextSize = 14f;
        this.layoutBorderSize = 1f;
        this.layoutBorderColor = Color.parseColor("#5AC0D9");
        //this.deleteIcon = "x";
        //this.isDeletable = true;
    }

    public void setSelected() {
        this.isSelected = true;
        this.tagTextColor = Color.parseColor("#033F4C");
        this.layoutColor = Color.parseColor("#88D315");
        //this.layoutColorPress = Color.parseColor("#ffc107");
        //this.isDeletable = false;
    }

    public void selectTag() {
        if (this.isSelected) {
            setDefault();
        }
        else {
            setSelected();
        }
    }
}
