package nullpointers.semanticintuition;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cunoraz.tagview.OnTagClickListener;
import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DefineProblem extends AppCompatActivity {

    TagView tagGroup;
    Button next;
    EditText problemDescEditText;

    private ProblemData problemData;


    /**
     * Create the current Define Problem screen everything that goes in here is created when the Define Problem Screen is shown
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_define_problem);

        //cast all GUI elements
        next = (Button) findViewById(R.id.goToProblemSession);
        tagGroup = (TagView) findViewById(R.id.tag_group);
        problemDescEditText = (EditText) findViewById(R.id.problemDescEditText);

        //Load problem data from intent
        problemData = ProblemData.loadProblemDataFromIntent(this);
        loadProblemData();


        //get to next window Problem if OK button is clicked
        next.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getBaseContext();
                //Check if there is a problem description
                if ("".equals(problemDescEditText.getText().toString())) {
                    Toast.makeText(context, context.getString(R.string.fillProblemDescPlease), Toast.LENGTH_LONG).show();
                    return;
                }
                Intent next = new Intent(DefineProblem.this, Problem.class);

                saveProblemData();
                problemData.loadProblemDataToIntent(next);
                startActivity(next);
            }
        });


        //Just writing all Tags to the DefineProblem Screen when user starts application
        try {
            //gets all Tags as a ArrayList<List> and writes each string in one tag
            //tag is added to the tagGroup and shown
            for (String tagFromDB: PhotoAndTags.getAllTags()) {
                if (!"".equals(tagFromDB)) {
                    CustomTag tag = new CustomTag(tagFromDB);
                    tagGroup.addTag(tag);
                }
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Please take a pictures and enter some tags first", Toast.LENGTH_SHORT).show();

        }

        tagGroup.setOnTagClickListener(new OnTagClickListener() {
            @Override
            public void onTagClick(Tag tag, int position) {
                ((CustomTag) tag).selectTag();
                redrawTagView(tagGroup);
            }
        });
    }

    private void redrawTagView(TagView tagGroup) {
        //redraw all tags
        List<Tag> tags = tagGroup.getTags();
        //We add a dummy tag and then remove it to redraw the tags
        tags.add(new Tag("dummyTag"));
        tagGroup.remove(tags.size()-1);
    }

    private void loadSelectedTagsFromDatabase() {
        List<String> selectedTags = DataManipulator.stringBackToList(problemData.problemTags);
        List<Tag> tags = tagGroup.getTags();
        //Go through the list of tags in the tagView and select the ones in selected tags
        for (Tag tag: tags) {
            if (selectedTags.contains(tag.text))
                ((CustomTag) tag).setSelected(); //Here we set selected instead of using selectTag
        }
    }

    private String selectedTagsToString() {
        List<String> selectedTags = new ArrayList<>();
        List<Tag> tags = tagGroup.getTags();
        for (Tag tag: tags) {
            if (((CustomTag) tag).isSelected) {
                selectedTags.add(tag.text);
            }
        }
        String[] selectedTagsArray = selectedTags.toArray(new String[0]);
        return Arrays.toString(selectedTagsArray);
    }

    protected void onStop() {
        super.onStop();
        saveProblemData(); //Save the data, before app is killed
    }

    protected void onStart() {
        super.onStart();
        loadProblemData();
    }

    //protected void onPause() {
    //    super.onPause(); // Always call the superclass method first
    //    saveProblemData(); //Save the data, before app is hidden or killed
    //}

    //protected void onResume() {
    //    super.onResume(); // Always call the superclass method first
    //    loadProblemData(); //Reload all data from the database on resume
    //}

    private void saveProblemData() {
        problemData.problemDescription = problemDescEditText.getText().toString();
        problemData.problemTags = selectedTagsToString();
        if (problemData.isEmpty())
            problemData.delete();
        else
            problemData.save();
    }

    private void loadProblemData() {
        problemData.reloadFromDatabase();
        problemDescEditText.setText(problemData.problemDescription);
        //Show selected tags after all the tags have been loaded to the tagview
        //we have to check that the problem has tags, if not, then nothing to load
        if (problemData.problemTags != null)
            if (!problemData.problemTags.isEmpty())
                loadSelectedTagsFromDatabase();
    }

    @Override
    public void onBackPressed() {
        //Here we have to imput a save mechanism
        Toast toast = Toast.makeText(getApplicationContext(), "Saving data", Toast.LENGTH_SHORT);
        toast.show();
        saveProblemData();
        super.onBackPressed();
    }

}
