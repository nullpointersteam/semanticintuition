package nullpointers.semanticintuition;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProblemList extends AppCompatActivity {

    private ListView problemsListView;
    private List<ProblemData> problemDataList;
    private ProblemDataAdapter adapter;
    private Runnable runOnFreakingUI;

    //GUI elements
    FloatingActionButton defineProblemButton;
    FloatingActionButton takePictureButton;
    FloatingActionButton viewGalleryButton;
    TextView sampleProblem;
    private ImageView imageHolder;
    private final int requestCode = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_list);

        //Cast GUI elements
        defineProblemButton = (FloatingActionButton) findViewById(R.id.defineProblemImageButton);
        takePictureButton = (FloatingActionButton) findViewById(R.id.takePictureImageButton);
        viewGalleryButton = (FloatingActionButton) findViewById(R.id.viewGalleryImageButton);
        imageHolder = (ImageView) findViewById(R.id.captured_photo);
        sampleProblem = (TextView) findViewById(R.id.sampleProblem);

        problemsListView = (ListView) findViewById(R.id.problemsListView);
        problemDataList = SugarRecord.listAll(ProblemData.class);

        adapter = new ProblemDataAdapter(this, problemDataList);
        problemsListView.setAdapter(adapter);


        //SET click listeners
        defineProblemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loadDefineProblemActivity = new Intent(ProblemList.this, DefineProblem.class);
                ProblemData.getNewProblemData().loadProblemDataToIntent(loadDefineProblemActivity);
                startActivity(loadDefineProblemActivity);
            }
        });


        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String filename = currentDateFormat() + ".jpg";
                File file = new File(getExternalFilesDir(null) + File.separator + filename);
                //Log.e("message", String.valueOf(getExternalFilesDir(null)));
                Uri uri = Uri.fromFile(file);
                Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(photoCaptureIntent, requestCode);
            }
        });

        viewGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProblemList.this, GridActivity.class);
                startActivity(intent);
            }
        });

        problemsListView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent DefineProblemIntent = new Intent(ProblemList.this, DefineProblem.class);
                String creationDate = ((TextView) view.findViewById(R.id.tvDateModified)).getText().toString();
                //Freaking freakers or the ORM change the name from myField to my_field, freakers!
                ProblemData problemData = ProblemData.find(ProblemData.class,"creation_date = ?",creationDate).get(0);
                problemData.loadProblemDataToIntent(DefineProblemIntent);
                startActivity(DefineProblemIntent);
            }
        });

        problemsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                String creationDate = ((TextView) view.findViewById(R.id.tvDateModified)).getText().toString();
                final ProblemData problemData = ProblemData.find(ProblemData.class,"creation_date = ?",creationDate).get(0);

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                problemData.delete();
                                runOnUiThread(runOnFreakingUI);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Do you want to delete this problem?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                return true;
            }
        });

        //Freaking listview can only be updated on UI thread
        runOnFreakingUI = new Runnable() {
            public void run() {
                problemDataList = SugarRecord.listAll(ProblemData.class);
                adapter.clear();
                if (problemDataList.isEmpty()) {
                    sampleProblem.setVisibility(View.VISIBLE);
                }
                else {
                    sampleProblem.setVisibility(View.GONE);
                    adapter.addAll(problemDataList);
                }
                //adapter.notifyDataSetChanged();
                //problemsListView.invalidateViews();
                //problemsListView.refreshDrawableState();
            }
        };
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        runOnUiThread(runOnFreakingUI);
    }

    public class ProblemDataAdapter extends ArrayAdapter<ProblemData> {
        // View lookup cache
        private class ViewHolder {
            TextView problemDesc;
            TextView modificationDate;
        }

        public ProblemDataAdapter(Context context, List<ProblemData> problems) {
            super(context, R.layout.problem_list_item, problems);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            ProblemData problem = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            ViewHolder viewHolder; // view lookup cache stored in tag
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.problem_list_item, parent, false);
                viewHolder.problemDesc = (TextView) convertView.findViewById(R.id.tvProblemDesc);
                viewHolder.modificationDate = (TextView) convertView.findViewById(R.id.tvDateModified);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            // Populate the data into the template view using the data object
            viewHolder.problemDesc.setText(problem.problemDescription);
            viewHolder.modificationDate.setText(problem.creationDate);
            // Return the completed view to render on screen
            return convertView;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.requestCode == requestCode && resultCode == RESULT_OK) {
            Intent loadTagsActivity = new Intent(this, TagsAfterCameraActivity.class);
            startActivity(loadTagsActivity);
        }
    }

    private String currentDateFormat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }
}
