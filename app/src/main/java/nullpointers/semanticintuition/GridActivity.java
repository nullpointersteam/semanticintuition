package nullpointers.semanticintuition;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

public class GridActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        ArrayList<URI> uriArrayList = new ArrayList<>();
        for(File file:(new File(String.valueOf(getExternalFilesDir(null)))).listFiles())
        {
            uriArrayList.add(file.toURI());
        }

        if(uriArrayList.isEmpty())
            Toast.makeText(getApplicationContext(), "No images found!", Toast.LENGTH_LONG).show();


        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this, uriArrayList));
    }

}
