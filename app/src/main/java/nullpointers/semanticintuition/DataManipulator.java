package nullpointers.semanticintuition;

import android.support.v4.util.SparseArrayCompat;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Gabo10 on 06/07/2016.
 */
public class DataManipulator {
    public static final String DELIMITER = ">-_<";

    public static List<String> stringBackToList(String stringToProcess) {
        return Arrays.asList(stringBackToArray(stringToProcess));
    }

    public static String[] stringBackToArray(String stringToProcess) {
            return stringToProcess.substring(1, stringToProcess.length() - 1).split(", ");
    }

    /**
     * We convert it from String to array, to list and finally to a set
     * @param stringToProcess
     * @return
     */
    public static Set<String> stringBackToSet(String stringToProcess) {
        return new LinkedHashSet<>(stringBackToList(stringToProcess));
    }

    /**
     * Takes a string array and generates a string in which each element is separated by the delimiter
     * @param stringArrayToProcess
     * @param delimiter
     * @return
     */
    public static String stringArrayToDelimitedString(String[] stringArrayToProcess, String delimiter) {
        StringBuilder outputString = new StringBuilder();
        for (String element : stringArrayToProcess) {
                outputString.append(element).append(delimiter);
        }
        return outputString.toString();
    }

    /**
     * Overloaded version, it takes a default delimiter defined in the class
     * @param stringArrayToProcess
     * @return
     */
    public static String stringArrayToDelimitedString(String[] stringArrayToProcess) {
        return stringArrayToDelimitedString(stringArrayToProcess, DELIMITER);
    }


    /**
     * Gets a string that has been delimited and returns a string array
     * @param stringToProcess
     * @param delimiter
     * @return
     */
    public static String[] delimitedStringToStringArray(String stringToProcess, String delimiter) {
        return stringToProcess.split(delimiter);
    }

    /**
     * Overloaded version for lazy people that do not want to specify a delimiter
     * @param stringToProcess
     * @return
     */
    public static String[] delimitedStringToStringArray(String stringToProcess) {
        return delimitedStringToStringArray(stringToProcess, DELIMITER);
    }


    /**
     * Here we make sure to obtain elements that are complete (URI, Answer)
     * @param imagesAnswers SparseArray object where we save the data
     * @param index 0 for URI, 1 for Answer
     * @return a concatenated string with all the values we have there.
     */
    public static String getStringFromImageAnswers(SparseArrayCompat<String[]> imagesAnswers, int index) {
        String resultString = "";
        for(int i = 0; i < imagesAnswers.size(); i++) {
            String[] elementArray = imagesAnswers.get(i);
            if (elementArray != null)
                //Check that both values (URI, Answer) are not empty
                if (!elementArray[0].isEmpty() && !elementArray[1].isEmpty())
                    resultString += elementArray[index] + DELIMITER;
        }
        return resultString;
    }


    /**
     * This is a special method to return a sparce array that contains the picturesPaths and answers
     * @param picturePaths
     * @param answers
     * @return
     */
    public static SparseArrayCompat<String[]> getSparceArrayFromStrings(String[] picturePaths, String[] answers) {
        SparseArrayCompat<String[]> resultSparceArray = new SparseArrayCompat<String[]>();
        String pictureElement = null;
        String answerElement = null;
        for (int i = 0; i < answers.length; i++) {
            pictureElement = (picturePaths[i] == null) ? "": picturePaths[i];
            answerElement = (answers[i] == null) ? "": answers[i];
            resultSparceArray.put(i,new String[] {pictureElement,answerElement});
        }
        return resultSparceArray;
    }

    /**
     * Gets a delimited string, and produces an output string that looks like "1, 2, 3, 4, 5"
     * @param delimitedString
     * @param delimiter
     * @return
     */
    public static String delimitedStringToOutputString(String delimitedString, String delimiter) {
        if (delimitedString == null || delimitedString.isEmpty()) {
            return "";
        }

        String resultString = Arrays.toString(delimitedStringToStringArray(delimitedString, delimiter));
                //delimitedStringToStringArray(delimitedString, delimiter).toString();
        resultString = resultString.substring(1, resultString.length() - 1);
        return resultString;
    }

    /**
     * Overloaded version with default delimiter
     * @param delimitedString
     * @return
     */
    public static String delimitedStringToOutputString (String delimitedString) {
        return delimitedStringToOutputString(delimitedString,DELIMITER);
    }
}
