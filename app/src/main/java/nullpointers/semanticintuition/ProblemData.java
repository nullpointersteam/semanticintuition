package nullpointers.semanticintuition;

import android.app.Activity;
import android.content.Intent;

import com.orm.SugarRecord;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Axel Garcia on 11/06/2016.
 */
public class ProblemData extends SugarRecord {

    public String problemDescription;
    public String problemTags;
    public String answers;
    public String picturesPaths;
    public String creationDate;
    public String creationState; //Variable to know if its new or a modification. "new" and "modify"

    //empty constructor for ORM
    public ProblemData() {
    }

    //emptyConstructor to set date
    public ProblemData(String creationDate) {
        this.creationDate = creationDate;
    }

    //Normal class Creator
    public ProblemData(String problemDescription, String problemTags, String answers,
                       String picturesPaths, String creationDate, String creationState) { //, ArrayList<String> ImagesPaths) {
        this.problemDescription = problemDescription;
        this.problemTags = problemTags;
        this.answers = answers;
        this.picturesPaths = picturesPaths;
        this.creationDate = creationDate;
        this.creationState = creationState;
        //this.ImagesPaths = ImagesPaths; //We will not remember the images
    }

    //Constructor without explicit date
    public ProblemData(String problemDescription, String problemTags,
                       String answers, String picturesPaths, String creationState) {
        this(problemDescription, problemTags, answers, picturesPaths, CurrentDateTime(), creationState);
    }

    public void saveData() {
        this.creationState = "modify";
        this.save();
    }

    public static String CurrentDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss").format(new Date());
    }

    public String[] getTagsArray() {
        if ("".equals(this.problemTags) || "[]".equals(this.problemTags)) {
            return new String[0];
        }
        else {
            return DataManipulator.stringBackToArray(this.problemTags);
        }
    }

    //Deprecated. 06-Jul-2016
    public void saveProblemDataToDB() {
        //If the ID is null, then we are creating the problem for the first time
        if (this.getId() == null) {
            this.save();
        }
        else {
            ProblemData DBProblemData = ProblemData.find(ProblemData.class, "creation_date = ?", this.creationDate).get(0);
            DBProblemData.problemDescription = this.problemDescription;
            DBProblemData.problemTags = this.problemTags;
            DBProblemData.answers = this.answers;
            DBProblemData.picturesPaths = this.picturesPaths;
            DBProblemData.creationDate = this.creationDate;
            DBProblemData.creationState = this.creationState;
            DBProblemData.save();
        }
    }

    public static ProblemData loadProblemFromDatabase(String problemIdentifier) {
        return ProblemData.find(ProblemData.class, "creation_date = ?", problemIdentifier).get(0);
    }

    public static String getIdentifierFromNewProblemData() {
        return getNewProblemData().creationDate;
    }

    public static ProblemData getNewProblemData() {
        //We call the constructor that creates the current DateTime
        ProblemData problemData = new ProblemData("","","","","new");
        problemData.save();
        return problemData;
    }

    public static ProblemData loadProblemDataFromIntent(Activity activity) {
        String problemIdentifier = activity.getIntent().getStringExtra("PROBLEM_IDENTIFIER");
        return loadProblemFromDatabase(problemIdentifier);
    }

    public void loadProblemDataToIntent(Intent intent) {
        intent.putExtra("PROBLEM_IDENTIFIER", this.creationDate);
    }

    public void reloadFromDatabase() {
        ProblemData DBProblemData = ProblemData.find(ProblemData.class, "creation_date = ?", this.creationDate).get(0);
        this.problemDescription = DBProblemData.problemDescription;
        this.problemTags = DBProblemData.problemTags;
        this.answers = DBProblemData.answers;
        this.picturesPaths = DBProblemData.picturesPaths;
        this.creationState = DBProblemData.creationState;
    }

    public boolean isEmpty() {
        if (this.problemDescription.isEmpty() && this.answers.isEmpty() && this.picturesPaths.isEmpty()
                && this.creationState.equalsIgnoreCase("new")) {
            return true;
        }
        else {
            return false;
        }
    }

    public String[] getAnswersArray() {
        return DataManipulator.delimitedStringToStringArray(this.answers);
    }

    public String[] getPicturesPathsArray() {
        return DataManipulator.delimitedStringToStringArray(this.picturesPaths);
    }

    public String getAnswerOutputString() {
        return DataManipulator.delimitedStringToOutputString(this.answers);
    }
}
