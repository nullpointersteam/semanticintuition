package nullpointers.semanticintuition;

/**
 * Created by achin on 7/9/2016.
 */

import android.app.Activity;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class CustomList extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] answers;
    private final String[] imageURIs;

    public CustomList(Activity context, String[] answers, String[] imageURIs) {
        super(context, R.layout.list_single, answers);
        this.context = context;
        this.answers = answers;
        this.imageURIs = imageURIs;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(answers[position]);
        //Log.e("locations: ", imageURIs[position]);
        Glide.with(context)
                .load(imageURIs[position])
                .crossFade()
                .placeholder(R.drawable.loading)
                .into(imageView);
        return rowView;
    }
}
