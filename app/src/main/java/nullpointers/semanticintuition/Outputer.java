package nullpointers.semanticintuition;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Axel Garcia on 2016-05-10. Class with static methods to output the application information
 */
public class Outputer {
    public static final String DEFAULT_OUTPUT_PATH = Environment.getExternalStorageDirectory() + "/semanticIntuition";
    public static final String DEFAULT_OUTPUT_FILENAME = "outputFile.png";

    /**
     * Get any kind of view and transforms it into an image. It will be interesting for converting
     * layouts into an image
     * @param view the view we want to convert
     * @return returns a bitmap that can be shared or saved to a file
     */
    public static Bitmap ConvertViewToBitmap(View view)
    {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return bitmap;
    }

    /**
     * Gets a bitmap image and saves it to a specified location
     * @param bitmap the bitmap to save to a file
     * @param outputPath the path where we want to save the image in PNG format
     * @param outputFilename the filename we want the output file to have
     */
    public static void saveBitmapToFile(Bitmap bitmap, String outputPath, String outputFilename) {
        File dir = new File(outputPath);
        dir.mkdirs();
        File file = new File(dir, outputFilename);

        //TODO: The image can be null, so we should check it
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapData = bos.toByteArray();

        //write the bytes in file
        try {
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Overloaded method that is called when the user does not specify a path, then we use a
     * default path
     * @param bitmap the bitmap to save to a file
     */
    public static void saveBitmapToFile(Bitmap bitmap) {
        saveBitmapToFile(bitmap, DEFAULT_OUTPUT_PATH, DEFAULT_OUTPUT_FILENAME);
    }

    /**
     * It gets an string and the context of the application (use getContext()) and generates a
     * intent to send a message, it always prompts the user to choose an application
     * @param dataToSend the data to send to the application the user will choose
     * @param context current context we are in, (hint: use getContext()).
     * @return returns the intent ready to be called
     */
    public static Intent getIntentSendMessage(String dataToSend, Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, dataToSend);
        sendIntent.setType("text/plain");
        return Intent.createChooser(sendIntent, context.getString(R.string.chosenIntentTitle));
    }

    /**
     * Method to start the intent activity of sharing with another app
     * @param dataToSend the data to send to the application the user will choose
     * @param context current context we are in, (hint: use getContext()).
     */
    public static void sendMessage(String dataToSend, Context context) {
        context.startActivity(getIntentSendMessage(dataToSend,context));
    }
}
