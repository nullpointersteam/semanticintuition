package nullpointers.semanticintuition;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Moritz on 31/05/2016.
 * This class is giving access the tag Names from the Database
 * e.g. TagStringClass test = new TagStringClass(getApplicationContext());
 *      test.getTags;
 *      will return the String[] that contains all Tag names
 */
public class TagStringsClass {

    public Context context;

    public TagStringsClass(Context c){
        context = c;
    }

    public String[] getTags() {

        SQLiteDatabase TagDB = context.openOrCreateDatabase("TagDB",context.MODE_PRIVATE,null);

        /***This counts the number of rows in the table***/
        Cursor resultSet1 = TagDB.rawQuery("Select count(*) from TagTable;",null);
        resultSet1.moveToFirst();
        //Toast.makeText(this, resultSet1.getString(0), Toast.LENGTH_LONG).show();

        //An empty string is created to accommodate all the tags returned from the database, with repetions
        String buffer = new String("");

        //This cursor fetches all the tags from the table
        Cursor resultSet2 = TagDB.rawQuery("Select Tags from TagTable;",null);

        //Finally, we process the above cursor to fetch all the tags and concatenate into the buffer
        if (resultSet2.moveToFirst()) {
            do {
                buffer += resultSet2.getString(0);
            } while (resultSet2.moveToNext());
        }

        //Converting array with repetitions into a set without repitions
        Set<String> mySet = new HashSet<String>(Arrays.asList(buffer.split("\\s+")));

        return mySet.toArray(new String[mySet.size()]);
    }
}
